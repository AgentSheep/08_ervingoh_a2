﻿using NUnit.Framework;
using System;
using Assignment2;
namespace Assignment2Test
{
    [TestFixture()]
    public class Test
    {
        Game game;

        [SetUp]
        public void CreateGame()
        {
             game = new Game();
        }
        public void RollMany(int Roll, int pins)
        {
            for (int i = 0; i < Roll; i++)
            {
                game.Roll(pins);
            }
        }
        [Test]
        public void RollGutterGame()
        {
            {
                game.Roll(0);
            }
            Assert.That(game.Score,Is.EqualTo(0));
        }
        [Test]
        public void RollOnes()
        {
            {
                game.Roll(20);
            }
            Assert.That(game.Score, Is.EqualTo(20));
        }
        [Test]
        public void RollSpare()
        {
            game.Roll(9);
            game.Roll(1);

            RollMany(18, 1);

            Assert.That(game.Score, Is.EqualTo(29));
        }
       

    }
}
